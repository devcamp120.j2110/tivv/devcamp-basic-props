import { Component } from "react";

class Info extends Component {
    render() {
        const {firstName, lastName, favNumber} = this.props
        return(
            <div>
                My name is {firstName} {lastName}. My favourite number is {favNumber}
            </div>
        )
    }
}
export default Info;